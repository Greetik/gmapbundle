# README #


### What is this repository for? ###

* GmapBundle is a bundle to add a markers and gpx files to a project.

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before edit or create the markers.

##Add it to AppKernel.php.##
new \Greetik\GmapBundle\GmapBundle()


##In the config.yml you can add your own service##
gmap:
    markerpermsservice: app.gmap
    gpxpermsservice: app.gmap
    interface: AppBundle:Gmap
    markerinterface: AppBundle:Gmap_Marker
    gpxinterface: AppBundle:Gmap_Gpx
