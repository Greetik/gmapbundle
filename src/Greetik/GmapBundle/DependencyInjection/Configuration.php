<?php

namespace Greetik\GmapBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('gmap');

        $rootNode
            ->children()
                ->scalarNode('interface')->defaultValue('GmapBundle:Gmap')->end()
                ->scalarNode('markerpermsservice')->defaultValue('marker.tools')->end()
                ->scalarNode('markerinterface')->defaultValue('GmapBundle:Marker')->end()
                ->scalarNode('gpxpermsservice')->defaultValue('gpx.tools')->end()
                ->scalarNode('gpxinterface')->defaultValue('GmapBundle:Gpx')->end()
                ->scalarNode('path')->defaultValue('web/uploads')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
