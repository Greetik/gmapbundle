<?php

namespace Greetik\GmapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gpx
 *
 * @ORM\Table(name="gpx", indexes={
 *      @ORM\Index(name="title", columns={"title"}),  @ORM\Index(name="project", columns={"project"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\GmapBundle\Repository\GpxRepository")
 */
class Gpx
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
     /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="start", type="string", length=255, nullable=true)
     */
    private $start;

    /**
     * @var string
     *
     * @ORM\Column(name="end", type="string", length=255, nullable=true)
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer", nullable=true)
     */
    private $project;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param string $end
     *
     * @return Gpx
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Gpx
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set start
     *
     * @param string $start
     *
     * @return Gpx
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Gpx
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Gpx
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }
}
