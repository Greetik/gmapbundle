<?php

namespace Greetik\GmapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gmapconfig
 *
 * @ORM\Table(name="gmapconfig")
 * @ORM\Entity(repositoryClass="Greetik\GmapBundle\Repository\GmapconfigRepository")
 */
class Gmapconfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="project", type="integer", unique=true)
     */
    private $project;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float")
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lon", type="float")
     */
    private $lon;

    /**
     * @var int
     *
     * @ORM\Column(name="zoom", type="integer")
     */
    private $zoom;

    /**
     * @var string
     *
     * @ORM\Column(name="maptype", type="string", length=255)
     */
    private $maptype;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Gmapconfig
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return int
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return Gmapconfig
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param float $lon
     *
     * @return Gmapconfig
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set zoom
     *
     * @param integer $zoom
     *
     * @return Gmapconfig
     */
    public function setZoom($zoom)
    {
        $this->zoom = $zoom;

        return $this;
    }

    /**
     * Get zoom
     *
     * @return int
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * Set maptype
     *
     * @param string $maptype
     *
     * @return Gmapconfig
     */
    public function setMaptype($maptype)
    {
        $this->maptype = $maptype;

        return $this;
    }

    /**
     * Get maptype
     *
     * @return string
     */
    public function getMaptype()
    {
        return $this->maptype;
    }
}

