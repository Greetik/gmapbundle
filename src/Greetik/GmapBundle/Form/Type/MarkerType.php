<?php
    namespace Greetik\GmapBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarkerType
 *
 * @author Paco
 */
class MarkerType extends AbstractType{
    private $externals;
    
    public function buildForm(FormBuilderInterface $builder, array $options){
            foreach($options['_treesections'] as $p){ $this->externals[((is_array($p))?$p['name']:$p->getname())] = ((is_array($p))?$p['id']:$p->getId()); }
    
        
            $builder
                ->add('title')
                ->add('body', TextareaType::class, array('required'=>false))
                ->add('color', TextType::class , array('required'=>false))
                ->add('externalid', ChoiceType::class, array(
                'choices' => $this->externals,
                'required'=>false,
                'expanded'=>false,
                'multiple'=>false 
            ));
                            
    }
    
    public function getName(){
        return 'Marker';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\GmapBundle\Entity\Marker',
            '_treesections' => array()
        ));
    }
}

