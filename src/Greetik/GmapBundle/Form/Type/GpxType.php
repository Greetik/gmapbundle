<?php

namespace Greetik\GmapBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Description of GpxType
 *
 * @author Jaime
 */
class GpxType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('start')
                ->add('end')
                ->add('filename', FileType::class, array('mapped'=>false))
                ->add('title');
    }

    public function getName() {
        return 'Gpx';
    }

    public function getDefaultOptions(array $options) {
        return array('data_class' => 'Greetik\GmapBundle\Entity\Gpx');
    }

}
