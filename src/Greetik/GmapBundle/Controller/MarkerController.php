<?php

namespace Greetik\GmapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\GmapBundle\Entity\Marker;
use Greetik\GmapBundle\Form\Type\MarkerType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class MarkerController extends Controller {

    public function getmarkersAction($idproject) {
        return new Response(json_encode(array('errorCode' => 0, 'data' => $this->get($this->getParameter('gmap.markerpermsservice'))->getMarkersByProject($idproject, 'array'))), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * View an individual marker, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction(Request $request, $id, $editForm = '') {
        $marker = $this->get($this->getParameter('gmap.markerpermsservice'))->getMarker($id);
        $markerObject = $this->get('marker.tools')->getMarkerObject($id);
        if (!$editForm)
            $editForm = $this->createForm(MarkerType::class, $markerObject);
        return $this->render($this->getParameter('gmap.markerinterface').':view.html.twig', array(
                    'item' => $marker,
                    'new_form' => $editForm->createView(),
                    'option' => $request->get('option'),
                    'modifyAllow' => $this->get($this->getParameter('gmap.markerpermsservice'))->getMarkerPerm($markerObject, 'modify')
        ));
    }

    public function moveAction(Request $request) {
        if (!$request->get('id'))
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Falta el Identificador del marcador')), 200, array('Content-Type' => 'application/json'));

        $marker = $this->get('marker.tools')->getMarkerObject($request->get('id'));
        $marker->setLat($request->get('lat'));
        $marker->setLon($request->get('lon'));

        try {
            $this->get($this->getParameter('gmap.markerpermsservice'))->modifyMarker($marker);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Get the data of a new Marker by Marker and persis it
     * 
     * @param Marker $item is received by Marker Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $marker = new Marker();
        $newForm = $this->createForm(MarkerType::class, $marker);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    $config = $this->get($this->getParameter('gmap.markerpermsservice'))->getMapConfig($idproject);
                    $marker->setLat($config->getLat());
                    $marker->setLon($config->getLon());
                    $this->get($this->getParameter('gmap.markerpermsservice'))->insertMarker($marker, $this->getUser()->getId(), $idproject);
                } catch (\Exception $e) {
                    $newForm->addError(new FormError($e->getMessage()));
                    return $this->render($this->getParameter('gmap.markerinterface').':insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $idproject)));
            }
        }

        return $this->render($this->getParameter('gmap.markerinterface').':insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an marker
     * 
     * @param int $id is received by Get Request
     * @param Marker $item is received by Marker Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $markerObject = $this->get('marker.tools')->getMarkerObject($id);

        $editForm = $this->createForm(MarkerType::class, $markerObject);


        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('gmap.markerpermsservice'))->modifyMarker($markerObject);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirect($this->generateUrl('gmap_viewmarker', array('id' => $id)));
    }

    public function deleteAction(Request $request, $id) {
        $markerObject = $this->get('marker.tools')->getMarkerObject($id);
        $$marker = $this->get('marker.tools')->getMarkerObject($id);
        
        if ($request->getMethod() == "POST") {
            try{
                $this->get($this->getParameter('gmap.markerpermsservice'))->deleteMarker($markerObject);
            }catch(\Exception $e){
                $this->addFlash('error', $e->getMessage());
            }
            return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $marker['project'])));
        }
        return $this->redirect($this->generateUrl('gmap_viewmarker', array('id' => $id)));
    }

}
