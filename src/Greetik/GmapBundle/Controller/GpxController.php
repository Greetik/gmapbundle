<?php

namespace Greetik\GmapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\GmapBundle\Entity\Gpx;
use Greetik\GmapBundle\Form\Type\GpxType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class GpxController extends Controller {

    public function getgpxsAction($idproject) {
        return new Response(json_encode(array('errorCode' => 0, 'data' => $this->get($this->getParameter('gmap.gpxpermsservice'))->getGpxsByProject($idproject, 'array'))), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * View an individual gpx, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Jaime
     */
    public function viewAction($id, $editForm = '') {
        $gpx = $this->get($this->getParameter('gmap.gpxpermsservice'))->getGpx($id);
        $gpxObject = $this->get('gpx.tools')->getGpxObject($id);
        if (!$editForm)
            $editForm = $this->createForm(GpxType::class, $gpxObject);
        return $this->render('GmapBundle:Gpx:view.html.twig', array(
                    'item' => $gpx,
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get($this->getParameter('gmap.gpxpermsservice'))->getGpxPerm($gpxObject, 'modify')
        ));
    }

    /**
     * Get the data of a new Gpx by Gpx and persis it
     * 
     * @param Gpx $item is received by Gpx Request
     * @author Jaime
     */
    public function insertAction(Request $request, $idproject) {
        $gpx = new Gpx();
        $newForm = $this->createForm(GpxType::class, $gpx);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    //    dump($request->files->all()['Gpx']['filename']);
                    $this->get($this->getParameter('gmap.gpxpermsservice'))->insertGpx($gpx, $request->files->all()['gpx']['filename'], $idproject);
                } catch (\Exception $e) {
                    $newForm->addError(new FormError($e->getMessage()));
                    return $this->render('GmapBundle:Gpx:insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $idproject)));
            }
        }

        return $this->render('GmapBundle:Gpx:insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an Gpx
     * 
     * @param int $id is received by Get Request
     * @param Gpx $item is received by Gpx Request
     * @author Jaime
     */
    public function modifyAction(Request $request, $id) {
        $gpxObject = $this->get('gpx.tools')->getGpxObject($id);

        $editForm = $this->createForm(GpxType::class, $gpxObject);


        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('gmap.gpxpermsservice'))->modifyGpx($gpxObject);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirect($this->generateUrl('gmap_viewgpx', array('id' => $id)));
    }

    public function deleteAction(Request $request, $id) {
        $gpx = $this->get($this->getParameter('gmap.gpxpermsservice'))->getGpx($id);
        if ($request->getMethod() == "POST") {
            try{
            $this->get($this->getParameter('gmap.gpxpermsservice'))->deleteGpx($this->get('gpx.tools')->getGpxObject($id));
            }catch(\Exception $e){
                $this->addFlash('error', $e->getMessage());
                return $this->redirect($this->generateUrl('gmap_viewgpx', array('id' => $id)));
            }
            return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $gpx['project'])));
        }
        return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $gpx['project'])));
    }

}
