<?php

namespace Greetik\GmapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * Render all the posts of the user
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {

        return $this->render($this->getParameter('gmap.interface') . ':index.html.twig', array(
                    'data' => $this->get($this->getParameter('gmap.markerpermsservice'))->getMarkersByProject($idproject),
                    'gpxs' => $this->get($this->getParameter('gmap.gpxpermsservice'))->getGpxsByProject($idproject),
                    'item' => $this->get($this->getParameter('gmap.markerpermsservice'))->getMapConfig($idproject),
                    'idproject' => $idproject,
                    'insertAllow' => $this->get($this->getParameter('gmap.markerpermsservice'))->getMarkerPerm('', 'insert', $idproject)
        ));
    }

    public function saveAction(Request $request, $idproject) {
        if ($request->getMethod() == "POST") {
            try {
                $config = $this->get($this->getParameter('gmap.markerpermsservice'))->getMapConfig($idproject);
                $config->setLat($request->get('lat'));
                $config->setLon($request->get('lon'));
                $config->setZoom($request->get('zoom'));
                $config->setMaptype($request->get('type'));
                $this->get($this->getParameter('gmap.markerpermsservice'))->saveConfig($config);
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        }

        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
    }

}
