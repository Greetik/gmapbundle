<?php

namespace Greetik\GmapBundle\Services;
use Greetik\GmapBundle\Entity\Marker;
use Greetik\GmapBundle\Entity\Gmapconfig;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */

class Markertools {
    
    private $em;
    private $gpx;
    private $serializer;
    
    public function __construct($_entityManager, $_gpx, $_serializer)
    {
        $this->em = $_entityManager;
        $this->gpx = $_gpx;
        $this->serializer=$_serializer;
    }

    public function getMarkerPerm(){
        return true;
    }
    
    public function getMarkersByProject($project){
        return $this->em->getRepository('GmapBundle:Marker')->findByProject($project);
    }
    
    public function getMarkerObject($id){
       $marker = $this->em->getRepository('GmapBundle:Marker')->findOneById($id);
     
       if (!$marker) throw new \Exception('No se encuentra el marcador');
       
       return $marker; 
    }
    
    public function getMarker($id){
       $marker = $this->em->getRepository('GmapBundle:Marker')->getMarker($id);
     
       if (!$marker) throw new \Exception('No se encuentra el marcador');
       
       return $marker; 
    }
    
    public function modifyMarker($marker){       
       $this->em->persist($marker);
       $this->em->flush();
    }
    
    public function insertMarker($marker, $user, $project){
        $marker->setProject($project);
        
        $this->em->persist($marker);
        $this->em->flush();
    }
    
    
    public function deleteMarker($marker){
        $this->em->remove($marker);
        $this->em->flush();
    }
    
    public function getMapconfig($project) {
        $mapconfig = $this->em->getRepository('GmapBundle:Gmapconfig')->findOneByProject($project);
        if (!$mapconfig) {
            $mapconfig = new Gmapconfig();
            $mapconfig->setProject($project);
            $mapconfig->setLat('40.416775');
            $mapconfig->setLon('-3.703790');
            $mapconfig->setMaptype('HYBRID');
            $mapconfig->setZoom(8);

            $this->em->persist($mapconfig);
            $this->em->flush();
        }

        return $mapconfig;
    }

    public function saveConfig($mapconfig) {
        $this->em->persist($mapconfig);
        $this->em->flush();
    }
    
    public function getMap($project){
        $map = $this->em->getRepository('GmapBundle:Gmapconfig')->findOneByProject($project);
        $map = json_decode($this->serializer->serialize($map, 'json'), 1);
        $map['markers'] = $this->getMarkersByProject($project);
        $map['gpx'] = $this->gpx->getGpxsByProject($project);
        return $map;

    }
}
