<?php

namespace Greetik\GmapBundle\Services;
use Greetik\GmapBundle\Entity\Gpx;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */

class Gpxtools {
    
    private $em;
    private $rootdir;
    private $path;
    
    public function __construct($_entityManager, $_rootdir, $_path)
    {
        $this->em = $_entityManager;
        $this->rootdir=$_rootdir;
        $this->path=$_path;
    }

    public function getGpxPerm(){
        return true;
    }
    
    public function getGpxsByProject($project){
        return $this->em->getRepository('GmapBundle:Gpx')->findByProject($project);
    }
    
    public function getGpxObject($id){
       $gpx = $this->em->getRepository('GmapBundle:Gpx')->findOneById($id);
     
       if (!$gpx) throw new NotFoundHttpException('No se encuentra el marcador');
       
       return $gpx; 
    }
    
    public function getGpx($id){
       $gpx = $this->em->getRepository('GmapBundle:Gpx')->getGpx($id);
     
       if (!$gpx) throw new NotFoundHttpException('No se encuentra el marcador');
       
       return $gpx; 
    }
    
    public function modifyGpx($gpx){       
       $this->em->persist($gpx);
       $this->em->flush();
    }
    
    public function insertGpx($gpx, $uploadedfile, $project){
        $gpx->setProject($project);
        
        $auxext = explode('.', $uploadedfile->getClientOriginalName());
        $ext = $auxext[count($auxext) - 1];
        if ($ext != 'gpx')
            throw new \Exception('La extensión es incorrecta');
        $filename = sha1(uniqid(mt_rand(), true)) . '.' . $ext;

        $gpx->setFilename($filename);
        
        $path = $this->rootdir . '/..'.$this->path.'/gpx/';

        if (!file_exists($path))
            mkdir($path);
        $path.=$gpx->getId() . '/';
        if (!file_exists($path))
            mkdir($path);

        $uploadedfile->move($path, $filename);
        
        
        $this->em->persist($gpx);
        $this->em->flush();
    }
    
    
    public function deleteGpx($gpx){
        @unlink($this->rootdir . '/..'.$this->path.'/gpx/'.$gpx->getFilename());
        $this->em->remove($gpx);
        $this->em->flush();
    }
    
}
